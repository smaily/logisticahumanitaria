
package com.example.logisticahumanitaria;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import database.DbEstados;
import database.Estados;

public class MainActivity extends AppCompatActivity {

    private EditText edtNombre;
    private Estados savedEstado;
    private boolean estadosAgregados;
    private int id;
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        insertTen();

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean completo = true;
                if (edtNombre.getText().toString().equals("")) {
                    edtNombre.setError("Introduce el nombre");
                    completo = false;
                }

                if (completo) {

                    DbEstados source = new DbEstados(MainActivity.this);
                    source.openDatabase();

                    Estados nEstado = new Estados();
                    nEstado.setNombre(edtNombre.getText().toString());


                    if (savedEstado == null) {
                        source.insertEstado(nEstado);

                        Toast.makeText(MainActivity.this,R.string.mensaje,

                                Toast.LENGTH_SHORT).show();
                        limpiar();
                    } else {
                        source.updateEstado(nEstado,id);

                        Toast.makeText(MainActivity.this, R.string.mensajeEdit,

                                Toast.LENGTH_SHORT).show();
                        limpiar();
                    }

                    source.close();

                }
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,
                        ListaActivity.class);
                startActivityForResult(i, 0);
            }

        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        if (Activity.RESULT_OK == resultCode) {
            Estados estado = (Estados) data.getSerializableExtra("estado");
            savedEstado = estado;
            id = estado.get_ID();
            edtNombre.setText(estado.getNombre());
        }else{
            limpiar();
        }
    }

    public void insertTen() {
        DbEstados llenar = new DbEstados(MainActivity.this);
        llenar.openDatabase();
        if(llenar.allEstados().size() == 0){
            String[] estados = {"Sinaloa", "Aguascalientes", "Sonora", "Chiapas", "Jalisco",
                    "Hidalgo", "Nuevo Leon", "Tabasco", "Zacatecas", "Yucatan"};
            for (int x = 0; x < 10; x++) {
                Estados nEstado = new Estados();
                nEstado.setNombre(estados[x]);
                llenar.insertEstado(nEstado);
            }
        }
    }

    public void limpiar(){
        savedEstado = null;
        edtNombre.setText("");
    }
}






