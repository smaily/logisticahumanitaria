package database;
import android.provider.BaseColumns;

import com.example.logisticahumanitaria.MainActivity;


public class DefinirTabla {
    public DefinirTabla(){}
    public static abstract class Estados implements BaseColumns {
        public static final String TABLE_NAME = "estados";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
    }
}
