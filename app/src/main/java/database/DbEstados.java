package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DbEstados {
    Context context;
    EstadoAdapter mDbHelper;
    SQLiteDatabase db;
    String[] columnsToRead = new String[]{
            DefinirTabla.Estados._ID,
            DefinirTabla.Estados.COLUMN_NAME_NOMBRE
    };

    public DbEstados(Context context) {
        this.context = context;
        mDbHelper = new EstadoAdapter(this.context);
    }
    public void openDatabase() {
        db = mDbHelper.getWritableDatabase();
    }


    public long insertEstado(Estados c) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estados.COLUMN_NAME_NOMBRE, c.getNombre());
        return db.insert(DefinirTabla.Estados.TABLE_NAME, null, values);
    }
    public long updateEstados(Estados c,int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estados.COLUMN_NAME_NOMBRE, c.getNombre());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.Estados.TABLE_NAME , values,
                DefinirTabla.Estados._ID + " = " + id,null);
    }

    public long updateEstado(Estados c,int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estados.COLUMN_NAME_NOMBRE, c.getNombre());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.Estados.TABLE_NAME , values,
                DefinirTabla.Estados._ID + " = " + id,null);
    }

    public int borrarEstados(long id) {
        return db.delete(DefinirTabla.Estados.TABLE_NAME, DefinirTabla.Estados._ID + "=?", new String[]{String.valueOf(id)});
    }

    private Estados readEstados(Cursor cursor) {
        Estados c = new Estados();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        return c;
    }

    public Estados getEstados(long id) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Estados.TABLE_NAME, columnsToRead, DefinirTabla.Estados._ID + " = ?", new String[]{String.valueOf(id)}, null, null, null);
        c.moveToFirst();
        Estados contacto = readEstados(c);
        c.close();
        return contacto;
    }

    public ArrayList<Estados> allEstados() {
        Cursor cursor = db.query(DefinirTabla.Estados.TABLE_NAME, columnsToRead, null, null, null, null, null);
        ArrayList<Estados> contactos = new ArrayList<Estados>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Estados c = readEstados(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }

    public void close() {
        mDbHelper.close();
    }
}
