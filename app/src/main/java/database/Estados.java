package database;

import java.io.Serializable;

public class Estados implements Serializable {
    private int _ID;
    private int idMovil;
    private String nombre;


    public Estados(){
        this.set_ID(0);
        this.setIdMovil(0);
        this.setNombre("");
    }

    public Estados(int _ID, String nombre){
        this.set_ID(_ID);
        this.setIdMovil(getIdMovil());
        this.setNombre(nombre);
    }


    public int get_ID() {
        return _ID;
    }
    public void set_ID(int _ID) {
        this._ID = _ID;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdMovil() {
        return idMovil;
    }

    public void setIdMovil(int idMovil) {
        this.idMovil = idMovil;
    }
}
